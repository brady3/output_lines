#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
  int n = atoi(argv[1]);

  printf("Creating a with %d lines.\n", n);

  for(int i=0; i < n; i++) {
    printf("Line %d - 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890\n", i+1);
  }
  printf("done!\n");

  return 0;
}
